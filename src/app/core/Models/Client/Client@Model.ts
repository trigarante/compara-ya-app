export interface ClientModel {
    id?: number;
    idPais?: number;
    nombre?: string;
    paterno?: string;
    materno?: string;
    cp?: number;
    calle?: string;
    numInt?: string;
    numExt?: string;
    idColonia?: number;
    colonia?: string;
    genero?: string;
    telefonoFijo?: string;
    telefonoMovil?: string;
    correo?: string;
    fechaNacimiento?: string;
    fechaRegistro?: string;
    archivo?: string;
    archivoSubido?: number;
    curp?: string;
    rfc?: string;
    nombrePaises?: string;
    razonSocial?: number;
}
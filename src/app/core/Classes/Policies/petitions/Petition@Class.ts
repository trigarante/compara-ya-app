export class PetitionClass {
    petitionsArr = [
        {
            id: 0,
            title: "Instrucciones de pago",
            subtitle:

                "\n Estimado asegurado a continuación se mostrará las opciones para el pago de su póliza, le pedimos seleccionar su aseguradora y ubicar la cuenta concentradora del banco de su elección." +
                "\n\nUna vez concluido el pago, le pedimos por favor enviar el comprobante de pago legible al siguiente clave de correo junto con el número de su póliza; asistenciapagos@ahorraseguros.mx" +

                "\n\nSi deseas asesoría en relación a cómo realizar la operación, por favor comunicate al 5536024275 opción 2 donde con gusto te atenderemos" +

                "\n\nSelecciona tu Aseguradora" +
                "\n\nNota 1: Al seleccionar la aseguradora deberá salir la siguiente leyenda." +
                "\n\n Recuerda compartirnos tu comprobante de pago" +

                "\n\nNota 2: Al seleccionar la aseguradora que no tiene cuenta concentradora se abre el siguiente mensaje." +
                "\n\nLo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros",
            text: "",
            phone: "5536024275",
            icon: String.fromCharCode(0xf095),
            event: this.paymentInstructions
        },
        {
            id: 1,
            title: "Catálogo de facturación",
            subtitle:
                "Estimado asegurado, para solicitar tu factura por favor envíanos un correo a la siguiente clave de correo: asistenciapagos@ahorraseguros.mx" +
                "\n\n\nPor favor envía la solicitud con los siguientes datos:" +
                "\n\nNombre completo" +
                "\nNúmero de póliza" +
                "\nNombre de tu aseguradora" +
                "\nTeléfono de contacto" +
                "\n\n\nEs importante que valides que tu RFC esté correcto en la póliza, en caso de tener una observación selecciona “modificar póliza existente” dentro del menú de opciones",
            text: "",
            phone: "",
            icon: "",
            event: this.billingCatalog
        },
        {
            id: 2,
            title: "Identificación oficial",
            subtitle: "¿Qué documentos son válidos para que pueda cargar la app?" +
                "\nLos documentos válidos como identificación oficial son:",
            text: `
            - INE (Vigente)
            - Pasaporte (Vigente)
            - Cédula Profesional`,
            phone: "",
            icon: "",
            event: this.officialIdentification
        },
        {
            id: 3,
            title: "Inspección vehicular",
            subtitle: "¡Ayúdanos a concretar tu inspección vehicular!" +
                "\n\n¿Qué es una inspección vehicular?" +
                "\n\nEs un requisito imprescindible para la contratación de tu seguro de auto, este proceso valida el estatus general de tu unidad con el fin de garantizar la protección del mismo durante la vigencia de la póliza y así poder brindarte un mejor servicio en caso de tener algún tipo de siniestro." +
                "\n\n¿Cómo lo hago?" +
                "\n\n1- Valida que hayas recibido un mensaje vía SMS y correo electrónico con la liga de inspección vehicular, en caso de no ser así, comunícate con nosotros:",
            text: "Marque la opción 6",
            phone: "5536024275",
            icon: String.fromCharCode(0xf095),
            event: this.vehicleInspection
        },
        {
            id: 4,
            title: "Aclaraciones y sugerencias",
            subtitle: "Comuníquese a nuestra línea para cualquier aclaración o sugerencia relacionada con su póliza",
            text: "",
            phone: "5588902464",
            icon: String.fromCharCode(0xf095),
            event: this.clarificationsAndSuggestions
        },
        {
            id: 5,
            title: "Cancelar póliza",
            subtitle: "Comuníquese a nuestra linea",
            text: "Opción 5",
            phone: "5536024275",
            icon: String.fromCharCode(0xf095),
            event: this.cancelPolicy
        }/* ,
        {
            id: 6,
            title: "Modificar póliza existente",
            subtitle: "",
            text: "",
            phone: "",
            icon: "",
            event: this.modifyPolicy
        },
        {
            id: 7,
            title: "En caso de siniestro",
            subtitle: "",
            text: "",
            phone: "",
            icon: "",
            event: this.eventOfClaim
        } */
    ];

    // Instrucciones de pago
    private paymentInstructions() {
        console.log("Instrucciones de pago");
    }

    // Catálogo de facturacion
    private billingCatalog() {
        console.log("Catálogo de facturación");
    }

    // Identificación oficial
    private officialIdentification() {
        console.log("Identificación oficial");
    }

    // Inspección vehicular
    private vehicleInspection() {
        console.log("Inspección vehicular");
    }

    // Aclaraciones y sugerencias
    private clarificationsAndSuggestions() {
        console.log("Aclaraciones y sugerencias");
    }

    // Cancelar póliza
    private cancelPolicy() {
        console.log("Cancelar póliza");
    }

    // Modificar póliza existente
    private modifyPolicy() {
        console.log("Modificar póliza");
    }

    // En caso de siniestro
    private eventOfClaim() {
        console.log("En caso de siniestro");
    }
}

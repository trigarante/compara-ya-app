import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { ConcentratorAccountComponent } from './components/accounts/concentratoraccount.component';
import { ViewDocumentComponent } from './components/view-document/view-document.component';
import { GeneralConditionsComponent } from "~/app/components/pages/support/components/general-conditions/general-conditions.component";

@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    exports: [],
    declarations: [
        ConcentratorAccountComponent,
        ViewDocumentComponent,
        GeneralConditionsComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SupportModule { }

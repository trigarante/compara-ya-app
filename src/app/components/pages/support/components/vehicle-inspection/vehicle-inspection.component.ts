import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { Animation, AnimationDefinition, isAndroid, isIOS, Image } from '@nativescript/core';
import { AnimationCurve } from '@nativescript/core/ui/enums';

import { CardConfig } from '~/app/core/Models/VehicleInspection/VehicleInspection@Model';


const ANIMATION_DURATION = 700;

@Component({
    selector: 'app-vehicle-inspection',
    templateUrl: 'vehicle-inspection.component.html',
    styleUrls: ['vehicle-inspection.component.scss']
})

export class VehicleInspectionComponent implements AfterViewInit {

    titleActionBar: string = 'Inspección Vehicular';

    isAndroid: boolean;
    isIOS: boolean;

    imageFrontCar: any;

    @Input() config: Array<CardConfig>;

    @ViewChild('front', { read: ElementRef, static: true }) front: ElementRef;
    @ViewChild('back', { read: ElementRef, static: true }) back: ElementRef;

    private isFront: boolean = true;
    private isAnimating: boolean = false;

    constructor(
        private routerExtensions: RouterExtensions
    ) {
        this.isAndroid = isAndroid;
        this.isIOS = isIOS;
    }

    ngAfterViewInit(): void {
        this.initializeCards();
    }

    flip(): void {
        if (!this.isAnimating) {
            this.isAnimating = true;
            if (this.isFront) {
                this.getFlipToBackAnimation().play().then(() => {
                    this.isFront = false;
                    this.isAnimating = false;
                }).catch(err => {
                    console.log('err', err);
                    this.initializeCards();
                    this.isAnimating = false;
                });
            } else {
                this.getFlipToFrontAnimation().play().then(() => {
                    this.isFront = true;
                    this.isAnimating = false;
                }).catch(err => {
                    console.log('err', err);
                    this.initializeCards();
                    this.isAnimating = false;
                });
            }
        }
    }

    private getFlipToBackAnimation(animationDuration: number = ANIMATION_DURATION): Animation {
        const animationDefinition: AnimationDefinition[] = [
            {
                target: this.front.nativeElement,
                rotate: { x: -180, y: 0, z: 0 },
                duration: animationDuration,
                curve: AnimationCurve.easeInOut
            },
            {
                target: this.back.nativeElement,
                rotate: { x: 0, y: 0, z: 0 },
                translate: { x: 0, y: 0 },
                duration: animationDuration,
                curve: AnimationCurve.easeInOut
            },
            {
                target: this.back.nativeElement,
                opacity: 1,
                delay: animationDuration / 2,
                duration: 1
            },
            {
                target: this.front.nativeElement,
                opacity: 0,
                delay: animationDuration / 2,
                duration: 1
            }
        ];
        return new Animation(animationDefinition, false);
    }

    private getFlipToFrontAnimation(animationDuration: number = ANIMATION_DURATION): Animation {
        const animationDefinition: AnimationDefinition[] = [
            {
                target: this.front.nativeElement,
                rotate: { x: 0, y: 0, z: 0 },
                duration: animationDuration,
                curve: AnimationCurve.easeInOut
            },
            {
                target: this.back.nativeElement,
                rotate: { x: 180, y: 0, z: 0 },
                duration: animationDuration,
                curve: AnimationCurve.easeInOut
            },
            {
                target: this.back.nativeElement,
                opacity: 0,
                delay: animationDuration / 2,
                duration: 1
            },
            {
                target: this.front.nativeElement,
                opacity: 1,
                delay: animationDuration / 2,
                duration: 1
            }
        ];
        return new Animation(animationDefinition, false);
    }

    initializeCards() {
        this.back.nativeElement.rotateX = 180;
        this.back.nativeElement.opacity = 0;
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    alertMessage() {
        alert({
            title: 'Alerta',
            message: 'Hola mundo',
            okButtonText: 'Aceptar',
            cancelable: true,
            cancelButtonText: 'Cancelar'
        });
    }

    // takePhotoCar() {
    //     /* if (camera.isAvailable()) { */
    //         camera.requestPermissions()
    //             .then((res: any) => {
    //                 console.log('Respuesta Then => ', res);
    //                 camera.takePicture()
    //                     .then((imageAsset: any) => {
    //                         console.log('El resultado es una instancia de ImageAsset');
    //                         const image = new Image();
    //                         image.src = imageAsset;
    //                         console.log('image.src => ', image.src);
    //                         this.imageFrontCar = imageAsset;
    //                         console.log('imageFrontCar => ', this.imageFrontCar);
    //                         this.initializeCards();
    //                     })
    //                     .catch((error) => {
    //                         console.error('Error => ', error.message);
    //                     })
    //             })
    //             .catch((error) => {
    //                 console.error('Respuesta Catch => ', error.message);
    //             });
    //     /* } */
    //     console.log('Camera Available => ', camera.isAvailable());
    //
    // }
}

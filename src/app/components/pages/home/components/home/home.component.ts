import { Component, OnInit } from '@angular/core';
import { Page } from '@nativescript/core';

import * as fileSystem from '@nativescript/core/file-system';

@Component({
    selector: 'app-home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.scss']
})

export class HomeComponent implements OnInit {

    PDF_URL: string = '';
    documentName: string = 'Folleto Explicativo Ahorra Seguros.pdf';

    constructor(
        private page: Page
    ) {
        this.page.actionBarHidden = true;
    }

    ngOnInit() {
        this.viewPDF();
    }

    viewPDF() {
        const currentAppFolder = fileSystem.knownFolders.currentApp();
        const filePath = fileSystem.path.join(currentAppFolder.path, 'assets', 'docs', 'carta-bienvenida', this.documentName);
        this.PDF_URL = filePath;
    }

    onLoad() {
        console.log('PDF Cargado correctamente');
    }
}
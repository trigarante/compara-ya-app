import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RouterExtensions } from '@nativescript/angular';
import { ApplicationSettings, isAndroid, isIOS, prompt, alert } from '@nativescript/core';
import {
    PromptResult,
    PromptOptions,
    inputType,
    capitalizationType,
} from '@nativescript/core/ui/dialogs/dialogs-common';
import { LoginModel } from '~/app/core/Models/Login/Login@Model';
import { AuthService } from '~/app/core/Services/Auth/Auth.service';


@Component({
    selector: 'app-security-password',
    templateUrl: 'password.component.html',
    styleUrls: ['password.component.scss']
})

export class SecurityPasswordComponent implements OnInit {

    titleActionBar: string = 'Cambiar Contraseña';

    isIOS: boolean;
    isAndroid: boolean;
    formGroup: FormGroup;

    confirmPass: boolean;
    userActive: number;
    isShowingPassword = false;

    constructor(
        private routerExtensions: RouterExtensions,
        private formBuilder: FormBuilder,
        private authService: AuthService
    ) {
        this.isIOS = isIOS;
        this.isAndroid = isAndroid;
        this.userActive = ApplicationSettings.getNumber('userActive');
    }

    goBack() {
        this.routerExtensions.backToPreviousPage()
    }

    ngOnInit() {
        this.startBuildForm();
    }

    confirmPassword() {
        let promptOptions: PromptOptions = {
            title: "Confirmación",
            message: "Escribe tu contraseña actual",
            okButtonText: "Aceptar",
            cancelButtonText: "Cancelar",
            cancelable: true,
            inputType: inputType.password, // email, number, text, password, or email
            capitalizationType: capitalizationType.sentences,
            // all. none, sentences or words
        };

        if (this.formGroup.valid) {

            const alertOptions = {
                title: 'Error de Confirmación',
                message: 'Contraseña Incorrecta',
                okButtonText: 'Aceptar',
            };

            if (this.passwordField.value === this.confirmPasswordField.value) {
                this.confirmPass = true;
                prompt(promptOptions)
                    .then((result: PromptResult) => {
                        // console.log(result.result);
                        if (result.result) {
                            const loginData: LoginModel = {
                                email: ApplicationSettings.getString('userEmail'),
                                contrasenaApp: result.text
                            };
                            this.authService.logIn(loginData)
                                .subscribe((response) => {
                                    // console.log(response);
                                }, (error) => {
                                    // console.log(error);
                                    alert(alertOptions);
                                }, () => {
                                    this.updatePasswordMethod();
                                });
                        }
                    });
            } else {
                this.confirmPass = false;
            }
        }
    }

    private updatePasswordMethod() {
        const alertOptions = {
            title: 'Contraseña Actualizada',
            message: 'La contraseña ha sido cambiada.',
            okButtonText: 'Aceptar',
        };
        this.authService.updatePassword(this.confirmPasswordField.value, this.userActive)
            .subscribe(
                data => {
                    // console.log(data);
                },
                error => {
                    // console.log(error);
                },
                () => {
                    alert(alertOptions).then(() => {
                        this.goBack();
                    });
                });
    }

    // Método para mostrar u ocultar la contraseña
    showPassword() {
        if (this.isShowingPassword) {
            this.isShowingPassword = false;
        } else {
            this.isShowingPassword = true;
        }
    }

    private startBuildForm(): void {
        this.formGroup = this.formBuilder.group({
            password: ["", [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}'), Validators.minLength(8), Validators.maxLength(32)]],
            confirmPassword: ["", [Validators.required]]
        });
    }

    get passwordField(): AbstractControl {
        return this.formGroup.get("password");
    }

    get confirmPasswordField(): AbstractControl {
        return this.formGroup.get("confirmPassword");
    }


}
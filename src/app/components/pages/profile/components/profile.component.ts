import { Component, OnInit } from '@angular/core';
import { ApplicationSettings, Observable, Page } from '@nativescript/core';
import { ClientModel } from '~/app/core/Models/Client/Client@Model';
import { DataClientService } from '~/app/core/Services/DataClient/dataclient.service';

import { Menu } from 'nativescript-menu';

@Component({
    selector: 'app-profile',
    templateUrl: 'profile.component.html',
    styleUrls: ['profile.component.scss']
})

export class ProfileComponent extends Observable implements OnInit {

    dataClientArr: ClientModel;
    userActive: number;

    constructor(
        private dataClientService: DataClientService,
        private page: Page
    ) {
        super();
        this.userActive = ApplicationSettings.getNumber('userActive');
        this.page.actionBarHidden = true;
        this.getDataClient();
    }

    ngOnInit() { }

    getDataClient() {
        let arrayDataClient: ClientModel;
        this.dataClientService.getDataClient(this.userActive)
            .subscribe((data: ClientModel) => {
                arrayDataClient = data;
            }, error => {
                console.error('Error Profile Get Data Client => ', error.message);
            }, () => {
                this.dataClientArr = arrayDataClient;
            })
    }

    showMenu() {
        Menu.popup({
            view: this.page.getViewById("btnMenu"),
            title: 'Configuración',
            cancelButtonText: 'Cerrar Menú',
            actions: [{ id: 0, title: 'Cambiar contraseña' }]
        })
            .then((action) => {
                console.log('Selected Menu Index => ', action.id + ' - ' + action.title);
            })
            .catch((error) => {
                console.log('error => ', error.message);
            })
    }
}
import { Component, OnInit } from '@angular/core';
import { isAndroid, isIOS, Page } from '@nativescript/core';

import * as TNSPhone from 'nativescript-phone';


@Component({
    selector: 'app-vehicle-inspection',
    templateUrl: 'vehicle-inspection.component.html',
    styleUrls: ['vehicle-inspection.component.scss']
})

export class VehicleInspectionComponent implements OnInit {

    isAndroid: boolean;
    isIOS: boolean;

    constructor(
        private page: Page
    ) {
        this.page.actionBarHidden = true;
        this.isAndroid = isAndroid;
        this.isIOS = isIOS;
    }

    ngOnInit() { }

    call(phone: string) {
        if (isAndroid) {
            TNSPhone.requestCallPermission()
                .then((response) => {
                    TNSPhone.dial(phone, true);
                })
                .catch((error) => {
                    TNSPhone.dial(phone, false);
                });
        }
        else if (isIOS) {
            TNSPhone.dial(phone, true);
        }
    }

}
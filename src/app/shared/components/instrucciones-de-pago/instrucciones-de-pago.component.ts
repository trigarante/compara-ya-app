import { Component, OnInit } from '@angular/core';
import { Page, isAndroid, isIOS } from '@nativescript/core';

import * as TNSPhone from 'nativescript-phone';

@Component({
    selector: 'instrucciones-de-pago-app',
    templateUrl: 'instrucciones-de-pago.component.html',
    styleUrls: ['instrucciones-de-pago.component.scss']
})

export class InstruccionesDePagoComponent implements OnInit {
    images: Array<any>;
    constructor(
        private page: Page
    ) {
        this.page.actionBarHidden = true;
    }

    ngOnInit() {
        this.selectImage();
    }

    selectImage() {
        const url: string = `~/assets/iconos-aseguradoras`;
        this.images = [
            { alias: "ABA", name: "aba.png", tel: '800 712 2828' },
            { alias: "SEGUROS AFIRME", name: "afirme.png", tel: '800 723 4763' },
            { alias: "ANA SEGUROS", name: "ana.png", tel: '800 911 2627 ' },
            { alias: "AXA", name: "axa.png", tel: '800 900 1292' },
            { alias: "BANORTE", name: "banorte.png", tel: '800 500 1500' },
            { alias: "GENERAL DE SEGUROS", name: "general.png", tel: '800 472 7696' },
            { alias: "GNP", name: "gnp.png", tel: '800 400 9000' },
            { alias: "HDI", name: "hdi.png", tel: '800 019 6000' },
            { alias: "INBURSA", name: "inbursa.png", tel: '800 909 0000' },
            { alias: "MAPFRE", name: "mapfre.png", tel: '55 5950 7777 ' },
            { alias: "MIGO", name: "migo.jpg", tel: '800 00 90 000' },
            { alias: "EL POTOSI", name: "elpotosi.png", tel: '800 00 90 000' },
            { alias: "QUALITAS", name: "qualitas.png", tel: '800 800 2880' },
            { alias: "ZURA", name: "sura.png", tel: '800 911 7692' },
            { alias: "ZURICH", name: "zurich.png", tel: '800 288 6911' },
            { alias: "EL AGUILA", name: "elaguila.png", tel: '800 705 8800' },
            { alias: "AIG", name: "aig.png", tel: '800 001 1300' },
            { alias: "LA LATINO", name: "lalatino.png", tel: '800 685 1170' }
        ];

        /* return `${url}/${images.find((e) => e.alias === alias).name}`; */
    }

    call(phone: string) {
        if (isAndroid) {
            console.log('Is Android');
            TNSPhone.requestCallPermission()
                .then((response) => {
                    TNSPhone.dial(phone, true);
                })
                .catch((error) => {
                    TNSPhone.dial(phone, false);
                });
        }
        else if (isIOS) {
            TNSPhone.dial(phone, true);
            console.log('Es iOS');
        }
    }
}
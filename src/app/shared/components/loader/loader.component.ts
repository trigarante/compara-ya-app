import { Component, Input, OnInit } from "@angular/core";
import { Page } from "@nativescript/core";

@Component({
    selector: "app-loader",
    templateUrl: "loader.component.html",
    styleUrls: ["loader.component.scss"]
})

export class LoaderComponent implements OnInit {
    // Input que recibe un texto personalizado que se mostrará debajo del loader cada que se haga uso de él.
    @Input() textLoader: string = "Cargando...";
    isBusy: boolean = true;

    constructor(
        private page: Page
    ) {
        this.page.actionBarHidden = true;
    }

    ngOnInit(): void { }
}
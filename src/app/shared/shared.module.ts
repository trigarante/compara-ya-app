import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";
import { FloatingButtonComponent } from "./components/floating-button/floating-button.component";
import { InstruccionesDePagoComponent } from "./components/instrucciones-de-pago/instrucciones-de-pago.component";
import { LoaderComponent } from "./components/loader/loader.component";
import { PrivacityComponent } from "./components/privacity/privacity.component";
import { SiniestrosComponent } from "./components/siniestros/siniestros.component";


@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    exports: [
        LoaderComponent,
        FloatingButtonComponent,
        PrivacityComponent,
        SiniestrosComponent,
        InstruccionesDePagoComponent
    ],
    declarations: [
        LoaderComponent,
        FloatingButtonComponent,
        PrivacityComponent,
        SiniestrosComponent,
        InstruccionesDePagoComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule { }
